﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSecondValueInArray.Exceptions
{
    public class NoSecondMaxValueException : Exception
    {
        public NoSecondMaxValueException()
        {

        }
        public NoSecondMaxValueException(string message)
        : base(message)
        {
        }
    }
}
