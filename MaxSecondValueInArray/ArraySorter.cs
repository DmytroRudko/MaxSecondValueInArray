﻿using MaxSecondValueInArray.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSecondValueInArray
{
    public class ArraySorter
    {
        public int[] SortArray(int[] incomeArray)
        {

            int temp;
            for (int i = 0; i < incomeArray.Length - 1; i++)
            {
                for (int j = i + 1; j < incomeArray.Length; j++)
                {
                    if (incomeArray[i] > incomeArray[j])
                    {
                        temp = incomeArray[i];
                        incomeArray[i] = incomeArray[j];
                        incomeArray[j] = temp;
                    }
                }
            }
            return incomeArray;
        }
    }
}
