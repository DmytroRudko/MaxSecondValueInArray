﻿using MaxSecondValueInArray.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSecondValueInArray
{
    public class MaxSecondValue
    {
        
        public int FoundMax2nd(int[] inputArray)
        {
            if (inputArray.Length < 2)
            {
                throw new NoSecondMaxValueException();
            }
            ArraySorter arraySorter = new ArraySorter();
            int[] sortedArray = arraySorter.SortArray(inputArray);
            
            for (int i = sortedArray.Length - 2; i >= 0 ; i--)
            {
                if (sortedArray[i] < sortedArray[sortedArray.Length -1])
                {
                    return sortedArray[i];
                }
            }
            throw new NoSecondMaxValueException();
        }

    }
}
