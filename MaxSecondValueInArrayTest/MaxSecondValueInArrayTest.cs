﻿using MaxSecondValueInArray;
using MaxSecondValueInArray.Exceptions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSecondValueInArrayTest
{
    [TestFixture]
    public class MaxSecondValueInArrayTest
    {
        private MaxSecondValue maxSecondValue;
        [SetUp]
        public void SetUp()
        {
            maxSecondValue = new MaxSecondValue();
        }

        [TestCase(new int[] { 0, 1 }, ExpectedResult = 0)]
        [TestCase(new int[] { -50, 50, -30, -160 }, ExpectedResult = -30)]
        [TestCase(new int[] { -50, 50, -30, -150, 50, 0 }, ExpectedResult = 0)]
        [TestCase(new int[] { -50, -150, -3000030, -150, -55, 0 }, ExpectedResult = -50)]
        [TestCase(new int[] { 1, 1, 0, 1, 1, 1 }, ExpectedResult = 0)]
        [TestCase(new int[] { 8, 5, 0, 8, 7, 7 }, ExpectedResult = 7)]
        public int IsReturnExpectedResult_IntegerArray_ReturnExpectedResult(int[] array)
        {
            return maxSecondValue.FoundMax2nd(array);
        }

        [TestCase(new int[] { })]
        [TestCase(new int[] { 5 })]
        [TestCase(new int[] { 5, 5 })]
        [TestCase(new int[] { -100, -100, -100 })]
        [TestCase(new int[] { 0, 0, 0, 0, 0, 0 })]
        public void ExceprionIfNoSecondMaxValue_ArrayWithOutSecondMaxValue_ReturnNoSecondMaxValueException(int[] array)
        {
            Assert.Throws<NoSecondMaxValueException>(() => maxSecondValue.FoundMax2nd(array));
        }
    }
}
